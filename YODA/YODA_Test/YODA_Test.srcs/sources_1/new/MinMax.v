`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 12.05.2022 11:33:28
// Design Name: 
// Module Name: MinMax
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module MinMax(
    input clk,
    input [15:0] Sample,
    input reg [7:0] Interval,
    output reg [15:0] Min,
    output reg [15:0] Max
    );
    
    reg [7:0] counter;
    initial begin
        Min = 16'hffff;
        Max = 0;
        counter = 0;
    end
    
    always @(posedge clk) begin
       
        
        if (counter > Interval) begin
           counter <= 0;
           Min <= 16'hffff;
           Max <= 0;
        end
        
        if (Sample > maxReg) begin
            Max  = Sample;
        end
        
        if (Sample < minReg) begin
            Min  = Sample;
        end
         counter ++;
        
        
    end   
 
endmodule
